# Desafio Sistema de Votação do Sicredi

Um projeto de demonstração criado como desafio sugerido pelo time da Sicredi, 
para o desenvolvimento de uma API para um simples sistema de votação.


## Depêndencias necessárias

- [x] [Maven 3](https://maven.apache.org/docs/3.6.3/release-notes.html)
- [x] [Java 11](https://openjdk.java.net/projects/jdk/11/)
- [x] [Postgres](https://www.postgresql.org/download/)

## Passos para execução

1 - Criação de database "voting" no postgres. A partir da criação do database 
os scripts de migração do flyway serão responsáveis pela criação da estrutura
necessária para execução do projeto normalmente. Também é possível a utilização
do banco de dados em memória H2, para isso é necessário iniciar o projeto com
a seguinte propriedade de VM: 

```bash
-Dspring.profiles.active=test
```

2 - Construir a aplicação com o maven

```bash
mvn clean package
```

3 - Executar a aplicação

```bash
mvn spring-boot:run
```

## Pilha de tecnologias

Este serviço se utiliza das seguintes ferramentas/tecnologias:

* **Spring Boot** - Framework open source para criação de aplicações autosuficientes;
* **Spring Doc Open API** - Bibloteca para geração automática de documentação dos serviços implementados;
* **H2** - Banco de dados embutido para armazenamento de dados em memória;
* **Postgres** - Banco de dados para persistência dos dados;
* **Flyway** - Migração de banco de dados, responsável pela criação e migração autómatica dos bancos de dados;

> Nota: O serviço de votação se utiliza da integração com a API externa 
> [user-info](https://user-info.herokuapp.com/users/50058790055) para consulta da elegibilidade de um usuário.

## Organização arquitetural do projeto

Em princípio, o projeto está subdividido nos pacotes `controller`, `service`, `repository`, `entity`, `dto`, 
`exceptions`, onde serão colocadas, respectivamente, as classes responsáveis por expor a API REST do serviço, 
as classes de serviço com as implementações das regras de negócio específicas da aplicação e tratamento das 
exceções exibidas para o consumidor, os repositórios responsáveis pelo acesso ao banco de dados 
(Spring Data JPA), as classes de entidades, objetos DTOs para comunicação com o consumidor, e por fim as classes de
exceção para facilitar o tratamento de erros.
No mais, deve-se modificar os arquivos de configuração (`.properties`, `pom.xml`, etc), de acordo com
as necessidades de projeto.

## Informações adicionais

A documentação via swagger pode ser encontrada nos seguintes endereços:

|Serviço | Endereço                          |
|--------|-----------------------------------|
|Swagger UI | http://localhost:8080/docs.html   |
|Swagger Docs | http://localhost:8080/v3/api-docs |

Um projeto do postman também se encontra disponível para importação no diretório 
[etc](https://gitlab.com/leonardonscsouza/desafio-sistema-votacao-sicredi/-/tree/main/etc).