package com.avenuecode.leonardo.polling.repository;

import com.avenuecode.leonardo.polling.entity.Poll;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PollRepository extends JpaRepository<Poll, Long> {

}
