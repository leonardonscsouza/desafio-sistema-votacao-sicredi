package com.avenuecode.leonardo.polling.repository;

import com.avenuecode.leonardo.polling.entity.Vote;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface VoteRepository extends JpaRepository<Vote, Long> {

    List<Vote> findAllByPollId(Long pollId);

    Optional<Vote> findByVoterIdAndPollId(String voterId, Long pollId);

}
