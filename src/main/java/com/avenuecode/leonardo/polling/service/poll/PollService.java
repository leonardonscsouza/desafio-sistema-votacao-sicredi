package com.avenuecode.leonardo.polling.service.poll;

import com.avenuecode.leonardo.polling.dto.PollDTO;
import com.avenuecode.leonardo.polling.dto.PollResultResponseDTO;
import com.avenuecode.leonardo.polling.exceptions.PollNotFoundException;
import com.avenuecode.leonardo.polling.exceptions.PollingAlreadyStartedException;
import com.avenuecode.leonardo.polling.exceptions.PollingFinishedException;

import java.time.LocalDateTime;
import java.util.List;

public interface PollService {

    List<PollDTO> getAllPolls();

    PollDTO addPoll(PollDTO pollDTO);

    PollDTO updatePoll(Long pollId, PollDTO pollDTO) throws PollNotFoundException;

    PollDTO startPolling(Long pollId, LocalDateTime endDate) throws PollNotFoundException,
            PollingAlreadyStartedException, PollingFinishedException;

    void deletePoll(Long pollId) throws PollNotFoundException;

    PollResultResponseDTO getPollResults(Long pollId) throws PollNotFoundException;
    
}
