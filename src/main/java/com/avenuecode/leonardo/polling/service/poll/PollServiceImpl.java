package com.avenuecode.leonardo.polling.service.poll;

import com.avenuecode.leonardo.polling.dto.PollDTO;
import com.avenuecode.leonardo.polling.dto.PollResultResponseDTO;
import com.avenuecode.leonardo.polling.entity.Poll;
import com.avenuecode.leonardo.polling.entity.Vote;
import com.avenuecode.leonardo.polling.exceptions.PollNotFoundException;
import com.avenuecode.leonardo.polling.exceptions.PollingAlreadyStartedException;
import com.avenuecode.leonardo.polling.exceptions.PollingFinishedException;
import com.avenuecode.leonardo.polling.repository.PollRepository;
import com.avenuecode.leonardo.polling.repository.VoteRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PollServiceImpl implements PollService {
    
    @Autowired
    private PollRepository pollRepository;

    @Autowired
    private VoteRepository voteRepository;

    private final ModelMapper modelMapper = new ModelMapper();
    
    private static final Logger LOGGER = LoggerFactory.getLogger(PollServiceImpl.class);

    @Override
    public List<PollDTO> getAllPolls() {
        List<Poll> polls = pollRepository.findAll();
        LOGGER.info("Returning all polls.");
        return polls.stream().map(poll -> modelMapper.map(poll, PollDTO.class)).collect(Collectors.toList());
    }

    @Override
    public PollDTO addPoll(PollDTO pollDTO) {
        Poll createdPoll = pollRepository.save(modelMapper.map(pollDTO, Poll.class));
        LOGGER.info("New poll registered [" + createdPoll.getName() + "].");
        return modelMapper.map(createdPoll, PollDTO.class);
    }

    @Override
    public PollDTO updatePoll(Long pollId, PollDTO pollDTO) throws PollNotFoundException {
        Optional<Poll> poll = pollRepository.findById(pollId);
        if(poll.isPresent()) {
            Poll updatedPoll  = poll.get();
            updatedPoll.setName(pollDTO.getName());
            updatedPoll.setStartingDate(pollDTO.getStartingDate());
            updatedPoll.setEndDate(pollDTO.getEndDate());
            Poll newPoll = pollRepository.save(updatedPoll);
            LOGGER.info("Poll updated [" + newPoll.getName() + "].");
            return modelMapper.map(newPoll, PollDTO.class);
        } else {
            LOGGER.error("Couldn't update the poll. " + PollNotFoundException.DEFAULT_MESSAGE);
            throw new PollNotFoundException();
        }
    }

    @Override
    public PollDTO startPolling(Long pollId, LocalDateTime endDate) throws PollNotFoundException,
            PollingAlreadyStartedException, PollingFinishedException {
        Optional<Poll> poll = pollRepository.findById(pollId);
        if(poll.isPresent()) {
            Poll updatedPoll  = poll.get();
            if(updatedPoll.getEndDate() != null) {
                if(updatedPoll.getEndDate().compareTo(LocalDateTime.now()) > 0) {
                    throw new PollingAlreadyStartedException();
                } else {
                    throw new PollingFinishedException();
                }
            }
            LocalDateTime timeNow = LocalDateTime.now();
            updatedPoll.setStartingDate(timeNow);
            if(endDate == null) {
                //Adding 1 minute to the current date as the end date of the poll
                updatedPoll.setEndDate(timeNow.plusMinutes(1));
            } else {
                updatedPoll.setEndDate(endDate);
            }
            Poll newPoll = pollRepository.save(updatedPoll);
            LOGGER.info("Poll [" + newPoll.getName() +  "] started and is finishing at [ " + newPoll.getEndDate() + " ].");
            return modelMapper.map(newPoll, PollDTO.class);
        } else {
            LOGGER.error("Couldn't start the poll. " + PollNotFoundException.DEFAULT_MESSAGE);
            throw new PollNotFoundException();
        }
    }

    @Override
    public void deletePoll(Long pollId) throws PollNotFoundException {
        Optional<Poll> poll = pollRepository.findById(pollId);
        if(poll.isPresent()) {
            pollRepository.delete(poll.get());
            LOGGER.info("Poll was deleted successfully.");
        } else {
            LOGGER.error("Couldn't delete the poll. " + PollNotFoundException.DEFAULT_MESSAGE);
            throw new PollNotFoundException();
        }
    }

    @Override
    public PollResultResponseDTO getPollResults(Long pollId) throws PollNotFoundException {
        Optional<Poll> poll = pollRepository.findById(pollId);
        if(poll.isPresent()) {
            List<Vote> votes = voteRepository.findAllByPollId(pollId);
            LOGGER.info("Returning all votes for the poll [" + poll.get().getName() + "].");
            Map<String, Long> result = new HashMap<>();
            result.put("Yes", 0L);
            result.put("No", 0L);
            for (Vote vote : votes) {
                if(vote.isChoice()){
                    result.put("Yes", result.get("Yes") + 1);
                } else {
                    result.put("No", result.get("No") + 1);
                }
            }
            return new PollResultResponseDTO(result);
        } else {
            LOGGER.error("Couldn't find the poll. " + PollNotFoundException.DEFAULT_MESSAGE);
            throw new PollNotFoundException();
        }
    }
}
