package com.avenuecode.leonardo.polling.service.external.usersapi;

import com.avenuecode.leonardo.polling.dto.UsersApiResponseDTO;
import com.avenuecode.leonardo.polling.exceptions.external.UserCheckFailedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class UsersApiServiceImpl implements UsersApiService {

    private RestTemplate restTemplate = new RestTemplate();

    private String API_BASE_URL = "https://user-info.herokuapp.com/users/{cpf}";

    private static final Logger LOGGER = LoggerFactory.getLogger(UsersApiServiceImpl.class);

    @Override
    public boolean canUserVote(String voterId) throws UserCheckFailedException {
        try {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            Map<String, String> params = new HashMap<>();
            params.put("cpf", voterId);
            ResponseEntity<UsersApiResponseDTO> response = restTemplate.exchange(
                    API_BASE_URL,
                    HttpMethod.GET,
                    new HttpEntity<>(null, httpHeaders),
                    new ParameterizedTypeReference<>() {},
                    params
            );
            return convertResult(response);
        } catch (Exception e) {
            LOGGER.error(UserCheckFailedException.DEFAULT_MESSAGE, e);
            throw new UserCheckFailedException();
        }
    }

    private boolean convertResult(ResponseEntity<UsersApiResponseDTO> response) throws UserCheckFailedException {
        if(response != null && response.getBody() != null) {
            UsersApiResponseDTO usersApiResponseDTO = response.getBody();
            if(usersApiResponseDTO.getStatus().equals("ABLE_TO_VOTE")) {
                return true;
            }
            return false;
        }
        throw new UserCheckFailedException();
    }
}
