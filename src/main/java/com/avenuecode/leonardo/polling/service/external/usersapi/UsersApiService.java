package com.avenuecode.leonardo.polling.service.external.usersapi;

import com.avenuecode.leonardo.polling.exceptions.external.UserCheckFailedException;

public interface UsersApiService {

    boolean canUserVote(String voterId) throws UserCheckFailedException;

}
