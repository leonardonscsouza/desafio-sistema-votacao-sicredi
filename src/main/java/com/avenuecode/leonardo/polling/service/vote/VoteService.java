package com.avenuecode.leonardo.polling.service.vote;

import com.avenuecode.leonardo.polling.dto.PollResultResponseDTO;
import com.avenuecode.leonardo.polling.dto.VoteDTO;
import com.avenuecode.leonardo.polling.dto.VoteRequestDTO;
import com.avenuecode.leonardo.polling.exceptions.*;
import com.avenuecode.leonardo.polling.exceptions.external.UserCheckFailedException;

import java.util.List;

public interface VoteService {

    List<VoteDTO> getAllVotesPerPoll(Long pollId) throws PollNotFoundException;

    VoteDTO addVote(Long pollId, VoteRequestDTO voteRequest) throws PollNotFoundException, PollingNotStartedException,
            PollingFinishedException, VoteAlreadyExistsException, UserCheckFailedException, VoterIneligibleException;
    
}
