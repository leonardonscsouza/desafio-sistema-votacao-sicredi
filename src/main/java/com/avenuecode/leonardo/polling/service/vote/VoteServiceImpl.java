package com.avenuecode.leonardo.polling.service.vote;


import com.avenuecode.leonardo.polling.dto.PollDTO;
import com.avenuecode.leonardo.polling.dto.PollResultResponseDTO;
import com.avenuecode.leonardo.polling.dto.VoteDTO;
import com.avenuecode.leonardo.polling.dto.VoteRequestDTO;
import com.avenuecode.leonardo.polling.entity.Poll;
import com.avenuecode.leonardo.polling.entity.Vote;
import com.avenuecode.leonardo.polling.exceptions.*;
import com.avenuecode.leonardo.polling.exceptions.external.UserCheckFailedException;
import com.avenuecode.leonardo.polling.repository.PollRepository;
import com.avenuecode.leonardo.polling.repository.VoteRepository;
import com.avenuecode.leonardo.polling.service.external.usersapi.UsersApiService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class VoteServiceImpl implements VoteService {

    @Autowired
    private PollRepository pollRepository;

    @Autowired
    private VoteRepository voteRepository;

    @Autowired
    private UsersApiService usersApiService;

    private final ModelMapper modelMapper = new ModelMapper();
    
    private static final Logger LOGGER = LoggerFactory.getLogger(VoteServiceImpl.class);


    @Override
    public List<VoteDTO> getAllVotesPerPoll(Long pollId) throws PollNotFoundException {
        Optional<Poll> poll = pollRepository.findById(pollId);
        if(poll.isPresent()) {
            List<Vote> votes = voteRepository.findAllByPollId(pollId);
            LOGGER.info("Returning all votes for the poll [" + poll.get().getName() + "].");
            return votes.stream().map(vote -> modelMapper.map(vote, VoteDTO.class)).collect(Collectors.toList());
        } else {
            LOGGER.error("Couldn't find the poll. " + PollNotFoundException.DEFAULT_MESSAGE);
            throw new PollNotFoundException();
        }
    }

    @Override
    public VoteDTO addVote(Long pollId, VoteRequestDTO voteRequest) throws PollNotFoundException,
            PollingNotStartedException, PollingFinishedException, VoteAlreadyExistsException, UserCheckFailedException,
            VoterIneligibleException {
        Optional<Poll> poll = pollRepository.findById(pollId);
        if(poll.isPresent()) {
            Poll currentPoll = poll.get();
            processVoteExceptions(voteRequest, currentPoll);
            Vote newVote = voteRepository.save(modelMapper.map(createNewVoteDTO(voteRequest, currentPoll), Vote.class));
            LOGGER.info("New vote registered for the poll [" + newVote.getPoll().getName() + "].");
            return modelMapper.map(newVote, VoteDTO.class);
        } else {
            LOGGER.error("Couldn't find the poll. " + PollNotFoundException.DEFAULT_MESSAGE);
            throw new PollNotFoundException();
        }
    }

    private void processVoteExceptions(VoteRequestDTO voteRequest, Poll currentPoll) throws PollingNotStartedException,
            PollingFinishedException, VoteAlreadyExistsException, UserCheckFailedException, VoterIneligibleException {
        if(currentPoll.getStartingDate() == null) {
            LOGGER.error("Couldn't vote on the poll. " + PollingNotStartedException.DEFAULT_MESSAGE);
            throw new PollingNotStartedException();
        } else if(currentPoll.getEndDate().compareTo(LocalDateTime.now()) < 0) {
            LOGGER.error("Couldn't vote on the poll. " + PollingFinishedException.DEFAULT_MESSAGE);
            throw new PollingFinishedException();
        }
        Optional<Vote> vote = voteRepository.findByVoterIdAndPollId(voteRequest.getVoterId(), currentPoll.getId());
        if(vote.isPresent()) {
            LOGGER.error("Couldn't vote on the poll. " + VoteAlreadyExistsException.DEFAULT_MESSAGE);
            throw new VoteAlreadyExistsException();
        }
        if(!usersApiService.canUserVote(voteRequest.getVoterId())) {
            LOGGER.error("Couldn't vote on the poll. " + VoterIneligibleException.DEFAULT_MESSAGE);
            throw new VoterIneligibleException();
        }
    }

    private VoteDTO createNewVoteDTO(VoteRequestDTO voteRequest, Poll poll) {
        VoteDTO voteDTO = new VoteDTO();
        voteDTO.setVoterId(voteRequest.getVoterId());
        voteDTO.setPoll(modelMapper.map(poll, PollDTO.class));
        voteDTO.setChoice(voteRequest.isChoice());
        voteDTO.setVoteDate(LocalDateTime.now());
        return voteDTO;
    }
}
