package com.avenuecode.leonardo.polling.exceptions;

public class PollingBaseException extends Exception {

    private final int errorCode;

    public PollingBaseException(String message, int errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
