package com.avenuecode.leonardo.polling.exceptions;

import org.springframework.http.HttpStatus;

public class PollingFinishedException extends PollingBaseException {


    public static String DEFAULT_MESSAGE = "The current poll is already finished.";


    public PollingFinishedException() {
        super(DEFAULT_MESSAGE, HttpStatus.CONFLICT.value());
    }

}
