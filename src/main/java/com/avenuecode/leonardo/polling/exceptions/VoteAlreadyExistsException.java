package com.avenuecode.leonardo.polling.exceptions;

import org.springframework.http.HttpStatus;

public class VoteAlreadyExistsException extends PollingBaseException {


    public static String DEFAULT_MESSAGE = "The voter already voted on the current poll.";


    public VoteAlreadyExistsException() {
        super(DEFAULT_MESSAGE, HttpStatus.CONFLICT.value());
    }

}
