package com.avenuecode.leonardo.polling.exceptions;

import org.springframework.http.HttpStatus;

public class PollingNotStartedException extends PollingBaseException {


    public static String DEFAULT_MESSAGE = "The current poll hasn't started yet.";


    public PollingNotStartedException() {
        super(DEFAULT_MESSAGE, HttpStatus.CONFLICT.value());
    }

}
