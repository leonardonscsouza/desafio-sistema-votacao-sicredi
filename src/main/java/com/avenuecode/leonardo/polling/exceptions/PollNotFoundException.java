package com.avenuecode.leonardo.polling.exceptions;

import org.springframework.http.HttpStatus;

public class PollNotFoundException extends PollingBaseException {


    public static String DEFAULT_MESSAGE = "Poll not found.";


    public PollNotFoundException() {
        super(DEFAULT_MESSAGE, HttpStatus.NOT_FOUND.value());
    }

}
