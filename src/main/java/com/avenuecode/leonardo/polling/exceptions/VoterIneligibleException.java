package com.avenuecode.leonardo.polling.exceptions;

import org.springframework.http.HttpStatus;

public class VoterIneligibleException extends PollingBaseException {


    public static String DEFAULT_MESSAGE = "The voter can't vote on the current poll.";


    public VoterIneligibleException() {
        super(DEFAULT_MESSAGE, HttpStatus.CONFLICT.value());
    }

}
