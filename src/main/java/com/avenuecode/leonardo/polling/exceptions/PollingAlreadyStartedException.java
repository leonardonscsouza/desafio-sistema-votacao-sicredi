package com.avenuecode.leonardo.polling.exceptions;

import org.springframework.http.HttpStatus;

public class PollingAlreadyStartedException extends PollingBaseException {


    public static String DEFAULT_MESSAGE = "The current poll already started.";

    public PollingAlreadyStartedException() {
        super(DEFAULT_MESSAGE, HttpStatus.CONFLICT.value());
    }

}
