package com.avenuecode.leonardo.polling.exceptions.external;

import com.avenuecode.leonardo.polling.exceptions.PollingBaseException;
import org.springframework.http.HttpStatus;

public class UserCheckFailedException extends PollingBaseException {


    public static String DEFAULT_MESSAGE = "The request to check if the voter is ineligible failed.";


    public UserCheckFailedException() {
        super(DEFAULT_MESSAGE, HttpStatus.INTERNAL_SERVER_ERROR.value());
    }

}
