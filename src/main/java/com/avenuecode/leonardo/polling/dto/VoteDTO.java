package com.avenuecode.leonardo.polling.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class VoteDTO {

    private long id;

    private String voterId;

    private PollDTO poll;

    private LocalDateTime voteDate;

    private boolean choice;

}
