package com.avenuecode.leonardo.polling.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class PollDTO {

    private long id;

    private String name;

    private LocalDateTime startingDate;

    private LocalDateTime endDate;

}
