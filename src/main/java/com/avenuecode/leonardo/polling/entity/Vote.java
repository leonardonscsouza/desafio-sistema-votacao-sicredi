package com.avenuecode.leonardo.polling.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "vote", schema = "app")
@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class Vote {

    @Id
    @SequenceGenerator(name="vote_sequence", sequenceName="app.vote_seq", allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="vote_sequence")
    private long id;

    @Column(name = "voterId")
    private String voterId;

    @OneToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "poll_id")
    private Poll poll;

    @Column(name = "vote_date")
    private LocalDateTime voteDate;

    @Column(name = "choice")
    private boolean choice;

}
