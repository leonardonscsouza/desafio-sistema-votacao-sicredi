package com.avenuecode.leonardo.polling.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "poll", schema = "app")
@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class Poll {

    @Id
    @SequenceGenerator(name="poll_sequence", sequenceName="app.poll_seq", allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="poll_sequence")
    private long id;

    @Column(name = "name", length = 30 , nullable = false, unique = true)
    private String name;

    @Column(name = "starting_date")
    private LocalDateTime startingDate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    @OneToMany(mappedBy = "poll", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<Vote> votes;

}
