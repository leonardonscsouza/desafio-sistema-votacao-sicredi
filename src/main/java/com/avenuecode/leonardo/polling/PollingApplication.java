package com.avenuecode.leonardo.polling;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.TimeZone;

@SpringBootApplication
public class PollingApplication {

	public static void main(String[] args) {
		TimeZone.setDefault(TimeZone.getTimeZone("America/Sao_Paulo"));
		SpringApplication.run(PollingApplication.class, args);
	}

	@Bean
	public OpenAPI challengeAPI() {
		return new OpenAPI()
				.info(new Info()
						.title("Sicredi - Desafio Sistema de Votação")
						.description("Um desafio solicitado pela equipe do Sicredi para criação de um sistema de votação")
						.version("v1.0.0.0")
						.license(
								new License()
										.name("Gitlab Repository")
										.url("https://gitlab.com/leonardonscsouza/desafio-sistema-votacao-sicredi")
						)
				);
	}

}
