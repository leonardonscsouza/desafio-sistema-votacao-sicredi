package com.avenuecode.leonardo.polling.controller;

import com.avenuecode.leonardo.polling.dto.PollDTO;
import com.avenuecode.leonardo.polling.dto.PollResultResponseDTO;
import com.avenuecode.leonardo.polling.dto.ResponseDTO;
import com.avenuecode.leonardo.polling.exceptions.PollNotFoundException;
import com.avenuecode.leonardo.polling.exceptions.PollingAlreadyStartedException;
import com.avenuecode.leonardo.polling.exceptions.PollingBaseException;
import com.avenuecode.leonardo.polling.exceptions.PollingFinishedException;
import com.avenuecode.leonardo.polling.service.poll.PollService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(path = "/api")
@Tag(name = "Polls", description = "Polls API")
public class PollController {

    private static final String APPOINTMENTS_V1 = "/v1.0/polls";

    private static final Logger LOGGER = LoggerFactory.getLogger(PollController.class);

    @Autowired
    private PollService pollService;

    @Operation(summary = "All polls", description = "Get all polls")
    @GetMapping(path = APPOINTMENTS_V1, produces = "application/json")
    public ResponseEntity<ResponseDTO<List<PollDTO>>> getAllPolls() {
        LOGGER.info("Consulting all polls...");
        return ResponseEntity.ok(new ResponseDTO<>(pollService.getAllPolls(), null));
    }

    @Operation(summary = "Include Poll", description = "Insert an poll on the system")
    @PostMapping(path = APPOINTMENTS_V1, produces = "application/json")
    public ResponseEntity<ResponseDTO<PollDTO>> addPoll(@RequestBody PollDTO pollDTO) {
        LOGGER.info("Adding poll...");
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(new ResponseDTO<>(pollService.addPoll(pollDTO), null));
    }

    @Operation(summary = "Update Poll", description = "Update an poll on the system")
    @PutMapping(path = APPOINTMENTS_V1 + "/{pollId}", produces = "application/json")
    public ResponseEntity<ResponseDTO<PollDTO>> updatePoll(@PathVariable("pollId") Long pollId,
                                                            @RequestBody PollDTO pollDTO)
            throws PollNotFoundException {
        LOGGER.info("Updating poll...");
        return ResponseEntity.ok(new ResponseDTO<>(pollService.updatePoll(pollId, pollDTO),
                null));
    }

    @Operation(summary = "Start Poll", description = "Start an polling on the system")
    @PutMapping(path = APPOINTMENTS_V1 + "/{pollId}/start", produces = "application/json")
    public ResponseEntity<ResponseDTO<PollDTO>> startPolling(@PathVariable("pollId") Long pollId,
                                                           @RequestParam(value = "endDate", required = false)
                                                           @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate)
            throws PollNotFoundException, PollingAlreadyStartedException, PollingFinishedException {
        LOGGER.info("Starting polling...");
        return ResponseEntity.ok(new ResponseDTO<>(pollService.startPolling(pollId, endDate),
                null));
    }

    @Operation(summary = "Delete Poll", description = "Delete an poll on the system")
    @DeleteMapping(path = APPOINTMENTS_V1 + "/{pollId}", produces = "application/json")
    public ResponseEntity<ResponseDTO<String>> deletePoll(@PathVariable("pollId") Long pollId)
            throws PollNotFoundException {
        LOGGER.info("Deleting poll...");
        pollService.deletePoll(pollId);
        return ResponseEntity.ok(new ResponseDTO("The poll was deleted successfully", null));
    }

    @Operation(summary = "Poll current result", description = "Get poll current result")
    @GetMapping(path = APPOINTMENTS_V1 + "/{pollId}/result", produces = "application/json")
    public ResponseEntity<ResponseDTO<PollResultResponseDTO>> getPollResult(@PathVariable("pollId") Long pollId)
            throws PollNotFoundException {
        LOGGER.info("Consulting result for poll...");
        return ResponseEntity.ok(new ResponseDTO<>(pollService.getPollResults(pollId), null));
    }

    @ExceptionHandler({ PollNotFoundException.class, PollingAlreadyStartedException.class, PollingFinishedException.class })
    private ResponseEntity<ResponseDTO> handlePollException(PollingBaseException e) {
        return ResponseEntity.status(e.getErrorCode()).body(new ResponseDTO(null, e.getMessage()));
    }

}
