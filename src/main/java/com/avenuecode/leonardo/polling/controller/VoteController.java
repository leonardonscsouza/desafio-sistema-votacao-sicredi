package com.avenuecode.leonardo.polling.controller;

import com.avenuecode.leonardo.polling.dto.ResponseDTO;
import com.avenuecode.leonardo.polling.dto.VoteDTO;
import com.avenuecode.leonardo.polling.dto.VoteRequestDTO;
import com.avenuecode.leonardo.polling.exceptions.*;
import com.avenuecode.leonardo.polling.exceptions.external.UserCheckFailedException;
import com.avenuecode.leonardo.polling.service.vote.VoteService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api")
@Tag(name = "Votes", description = "Votes API")
public class VoteController {

    private static final String VOTES_V1 = "/v1.0/votes";

    private static final Logger LOGGER = LoggerFactory.getLogger(VoteController.class);

    @Autowired
    private VoteService voteService;

    @Operation(summary = "All votes for poll", description = "Get all votes for poll")
    @GetMapping(path = VOTES_V1 + "/poll/{pollId}", produces = "application/json")
    public ResponseEntity<ResponseDTO<List<VoteDTO>>> getAllVotesForPoll(@PathVariable("pollId") Long pollId)
            throws PollNotFoundException {
        LOGGER.info("Consulting all votes for poll...");
        return ResponseEntity.ok(new ResponseDTO<>(voteService.getAllVotesPerPoll(pollId), null));
    }

    @Operation(summary = "Include Vote", description = "Insert an vote on the poll")
    @PostMapping(path = VOTES_V1 + "/poll/{pollId}", produces = "application/json")
    public ResponseEntity<ResponseDTO<VoteDTO>> addVote(@PathVariable("pollId") Long pollId,
                                                        @RequestBody VoteRequestDTO voteRequestDTO)
            throws PollNotFoundException, PollingNotStartedException, PollingFinishedException,
            VoteAlreadyExistsException, VoterIneligibleException, UserCheckFailedException {
        LOGGER.info("Adding vote...");
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(new ResponseDTO<>(voteService.addVote(pollId, voteRequestDTO), null));
    }

    @ExceptionHandler({ PollNotFoundException.class, PollingNotStartedException.class, PollingFinishedException.class,
            VoteAlreadyExistsException.class, UserCheckFailedException.class, VoterIneligibleException.class})
    private ResponseEntity<ResponseDTO> handlePollException(PollingBaseException e) {
        return ResponseEntity.status(e.getErrorCode()).body(new ResponseDTO(null, e.getMessage()));
    }

}
