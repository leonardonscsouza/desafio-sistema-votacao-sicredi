CREATE SCHEMA IF NOT EXISTS "app";

CREATE TABLE app.poll (
    id INTEGER PRIMARY KEY,
    name VARCHAR(30) NOT NULL,
    starting_date TIMESTAMP,
    end_date TIMESTAMP
);

CREATE SEQUENCE app.poll_seq START WITH 1 INCREMENT 1;

CREATE TABLE app.vote (
    id INTEGER PRIMARY KEY,
    voter_id VARCHAR(11) NOT NULL,
    poll_id INTEGER NOT NULL REFERENCES app.poll(id),
    vote_date TIMESTAMP,
    choice BOOLEAN NOT NULL
);

CREATE SEQUENCE app.vote_seq START WITH 1 INCREMENT 1;