package com.avenuecode.leonardo.polling.builder;

import com.avenuecode.leonardo.polling.dto.PollResultResponseDTO;
import com.avenuecode.leonardo.polling.dto.UsersApiResponseDTO;
import com.avenuecode.leonardo.polling.dto.VoteDTO;
import com.avenuecode.leonardo.polling.dto.VoteRequestDTO;
import com.avenuecode.leonardo.polling.entity.Vote;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class VoteBuilder {

    private static final Long id = 1L;

    private static final String voterId = "12345678901";

    private static final LocalDateTime voteDate = LocalDateTime.now();

    private static final boolean choice = true;

    private static final Long idAux = 2L;

    private static final String voterIdAux = "10987654321";

    private static final LocalDateTime voteDateAux = LocalDateTime.now();

    private static final boolean choiceAux = false;

    private static final PollBuilder pollBuilder = new PollBuilder();

    public Vote createVoteEntity() {
        return new Vote(id, voterId, pollBuilder.createPollEntity(), voteDate, choice);
    }

    public Vote createVoteEntityAux() {
        return new Vote(idAux, voterIdAux, pollBuilder.createPollEntity(), voteDateAux, choiceAux);
    }

    public VoteDTO createVoteDTO() {
        return new VoteDTO(id, voterId, pollBuilder.createPollDTO(), voteDate, choice);
    }

    public VoteDTO createVoteDTOAux() {
        return new VoteDTO(idAux, voterIdAux, pollBuilder.createPollDTOAux(), voteDateAux, choiceAux);
    }

    public List<Vote> createListVoteEntity() {
        List<Vote> polls = new ArrayList<>();
        polls.add(createVoteEntity());
        polls.add(createVoteEntityAux());
        return polls;
    }

    public List<VoteDTO> createListVoteDTO() {
        List<VoteDTO> polls = new ArrayList<>();
        polls.add(createVoteDTO());
        polls.add(createVoteDTOAux());
        return polls;
    }

    public VoteRequestDTO createVoteRequestDTO() {
        return new VoteRequestDTO(voterId, choice);
    }

    public VoteRequestDTO createVoteRequestAuxDTO() {
        return new VoteRequestDTO(voterIdAux, choiceAux);
    }

    public UsersApiResponseDTO createUsersApiResponseDTOAble() {
        return new UsersApiResponseDTO("ABLE_TO_VOTE");
    }

    public UsersApiResponseDTO createUsersApiResponseDTOUnable() {
        return new UsersApiResponseDTO("UNABLE_TO_VOTE");
    }

}
