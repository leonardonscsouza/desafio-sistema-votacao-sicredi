package com.avenuecode.leonardo.polling.builder;

import com.avenuecode.leonardo.polling.dto.PollDTO;
import com.avenuecode.leonardo.polling.dto.PollResultResponseDTO;
import com.avenuecode.leonardo.polling.entity.Poll;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PollBuilder {

    private static final Long id = 1L;

    private static final String name = "Poll";

    private static final LocalDateTime startingDate = LocalDateTime.now();

    private static final LocalDateTime endDate = LocalDateTime.now().plusMinutes(1);

    private static final Long idAux = 2L;

    private static final String nameAux = "Poll Aux";

    private static final LocalDateTime startingDateAux = null;

    private static final LocalDateTime endDateAux = null;

    public Poll createPollEntity() {
        return new Poll(id, name, startingDate, endDate, null);
    }

    public Poll createPollEntityAux() {
        return new Poll(idAux, nameAux, startingDateAux, endDateAux, null);
    }

    public PollDTO createPollDTO() {
        return new PollDTO(id, name, startingDate, endDate);
    }

    public PollDTO createPollDTOAux() {
        return new PollDTO(idAux, nameAux, startingDateAux, endDateAux);
    }

    public List<Poll> createListPollEntity() {
        List<Poll> polls = new ArrayList<>();
        polls.add(createPollEntity());
        polls.add(createPollEntityAux());
        return polls;
    }

    public List<PollDTO> createListPollDTO() {
        List<PollDTO> polls = new ArrayList<>();
        polls.add(createPollDTO());
        polls.add(createPollDTOAux());
        return polls;
    }

    public PollResultResponseDTO createPollResultResponseDTO() {
        Map<String, Long> result = new HashMap<>();
        result.put("Yes", 1L);
        result.put("No", 1L);
        return new PollResultResponseDTO(result);
    }

}
