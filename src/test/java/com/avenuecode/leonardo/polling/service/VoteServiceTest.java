package com.avenuecode.leonardo.polling.service;

import com.avenuecode.leonardo.polling.builder.PollBuilder;
import com.avenuecode.leonardo.polling.builder.VoteBuilder;
import com.avenuecode.leonardo.polling.dto.PollDTO;
import com.avenuecode.leonardo.polling.dto.PollResultResponseDTO;
import com.avenuecode.leonardo.polling.dto.VoteDTO;
import com.avenuecode.leonardo.polling.dto.VoteRequestDTO;
import com.avenuecode.leonardo.polling.entity.Poll;
import com.avenuecode.leonardo.polling.entity.Vote;
import com.avenuecode.leonardo.polling.exceptions.*;
import com.avenuecode.leonardo.polling.exceptions.external.UserCheckFailedException;
import com.avenuecode.leonardo.polling.repository.PollRepository;
import com.avenuecode.leonardo.polling.repository.VoteRepository;
import com.avenuecode.leonardo.polling.service.external.usersapi.UsersApiService;
import com.avenuecode.leonardo.polling.service.poll.PollService;
import com.avenuecode.leonardo.polling.service.vote.VoteService;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@ActiveProfiles("test")
public class VoteServiceTest {

    @MockBean
    private PollRepository pollRepository;

    @MockBean
    private VoteRepository voteRepository;

    @MockBean
    private UsersApiService usersApiService;

    @Autowired
    private VoteService voteService;

    private VoteBuilder voteBuilder = new VoteBuilder();
    private PollBuilder pollBuilder = new PollBuilder();

    @Test
    @DisplayName("Search all vote per poll - Success")
    void getAllVotesPerPoll() throws PollNotFoundException {
        Poll poll = pollBuilder.createPollEntity();
        List<Vote> votes = voteBuilder.createListVoteEntity();

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(poll));
        Mockito.when(voteRepository.findAllByPollId(Mockito.anyLong())).thenReturn(votes);

        List<VoteDTO> result = voteService.getAllVotesPerPoll(poll.getId());

        Assertions.assertNotNull(result);
        Assertions.assertNotEquals(result.size(), 0);

    }

    @Test
    @DisplayName("Search all vote per poll - Not Found")
    void getAllVotesPerPollNotFound() {
        PollDTO poll = pollBuilder.createPollDTO();

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());

        try {
            voteService.getAllVotesPerPoll(poll.getId());
        } catch (PollNotFoundException ex) {
            Assertions.assertNotNull(ex);
            Assertions.assertEquals(ex.getMessage(), PollNotFoundException.DEFAULT_MESSAGE);
        }
    }

    @Test
    @DisplayName("Add Vote - Success")
    void addVote() throws VoterIneligibleException, PollingNotStartedException, PollingFinishedException,
            VoteAlreadyExistsException, PollNotFoundException, UserCheckFailedException {
        Poll poll = pollBuilder.createPollEntity();
        poll.setEndDate(LocalDateTime.now().plusDays(1));
        Vote vote = voteBuilder.createVoteEntity();
        VoteRequestDTO voteRequestDTO = voteBuilder.createVoteRequestDTO();

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(poll));
        Mockito.when(voteRepository.findByVoterIdAndPollId(Mockito.anyString(), Mockito.anyLong())).thenReturn(Optional.empty());
        Mockito.when(usersApiService.canUserVote(Mockito.anyString())).thenReturn(true);
        Mockito.when(voteRepository.save(Mockito.any(Vote.class))).thenReturn(vote);

        VoteDTO result = voteService.addVote(poll.getId(), voteRequestDTO);

        Assertions.assertNotNull(result);
        Assertions.assertNotNull(result.getVoteDate());
    }

    @Test
    @DisplayName("Add Vote - Poll Not Found")
    void addVotePollNotFound() throws VoterIneligibleException, PollingNotStartedException, PollingFinishedException,
            VoteAlreadyExistsException, UserCheckFailedException {
        Poll poll = pollBuilder.createPollEntity();
        VoteRequestDTO voteRequestDTO = voteBuilder.createVoteRequestDTO();

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());

        try {
            voteService.addVote(poll.getId(), voteRequestDTO);
        } catch (PollNotFoundException ex) {
            Assertions.assertNotNull(ex);
            Assertions.assertEquals(ex.getMessage(), PollNotFoundException.DEFAULT_MESSAGE);
        }
    }

    @Test
    @DisplayName("Add Vote - Polling Not Started")
    void addVotePollingNotStarted() throws VoterIneligibleException, PollingFinishedException,
            VoteAlreadyExistsException, PollNotFoundException, UserCheckFailedException {
        Poll poll = pollBuilder.createPollEntityAux();
        VoteRequestDTO voteRequestDTO = voteBuilder.createVoteRequestDTO();

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(poll));

        try {
            voteService.addVote(poll.getId(), voteRequestDTO);
        } catch (PollingNotStartedException ex) {
            Assertions.assertNotNull(ex);
            Assertions.assertEquals(ex.getMessage(), PollingNotStartedException.DEFAULT_MESSAGE);
        }
    }

    @Test
    @DisplayName("Add Vote - Polling Finished")
    void addVotePollingFinished() throws PollNotFoundException, PollingNotStartedException,
            VoteAlreadyExistsException, UserCheckFailedException, VoterIneligibleException {
        Poll poll = pollBuilder.createPollEntity();
        poll.setEndDate(LocalDateTime.now().minusDays(1));
        VoteRequestDTO voteRequestDTO = voteBuilder.createVoteRequestDTO();

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(poll));

        try {
            voteService.addVote(poll.getId(), voteRequestDTO);
        } catch (PollingFinishedException ex) {
            Assertions.assertNotNull(ex);
            Assertions.assertEquals(ex.getMessage(), PollingFinishedException.DEFAULT_MESSAGE);
        }
    }

    @Test
    @DisplayName("Add Vote - Already voted")
    void addVoteAlreadyVoted() throws PollNotFoundException, PollingNotStartedException, PollingFinishedException,
            UserCheckFailedException, VoterIneligibleException {
        Poll poll = pollBuilder.createPollEntity();
        Vote vote = voteBuilder.createVoteEntity();
        VoteRequestDTO voteRequestDTO = voteBuilder.createVoteRequestDTO();

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(poll));
        Mockito.when(voteRepository.findByVoterIdAndPollId(Mockito.anyString(), Mockito.anyLong())).thenReturn(Optional.of(vote));

        try {
            voteService.addVote(poll.getId(), voteRequestDTO);
        } catch (VoteAlreadyExistsException ex) {
            Assertions.assertNotNull(ex);
            Assertions.assertEquals(ex.getMessage(), VoteAlreadyExistsException.DEFAULT_MESSAGE);
        }
    }

    @Test
    @DisplayName("Add Vote - User Check Failed")
    void addVoteUserCheckFailed() throws PollNotFoundException, PollingNotStartedException, PollingFinishedException,
            VoteAlreadyExistsException, VoterIneligibleException, UserCheckFailedException {
        Poll poll = pollBuilder.createPollEntity();
        poll.setEndDate(LocalDateTime.now().plusDays(1));
        Vote vote = voteBuilder.createVoteEntity();
        VoteRequestDTO voteRequestDTO = voteBuilder.createVoteRequestDTO();

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(poll));
        Mockito.when(voteRepository.findByVoterIdAndPollId(Mockito.anyString(), Mockito.anyLong())).thenReturn(Optional.empty());
        Mockito.when(usersApiService.canUserVote(Mockito.anyString())).thenThrow(new UserCheckFailedException());

        try {
            voteService.addVote(poll.getId(), voteRequestDTO);
        } catch (UserCheckFailedException ex) {
            Assertions.assertNotNull(ex);
            Assertions.assertEquals(ex.getMessage(), UserCheckFailedException.DEFAULT_MESSAGE);
        }
    }

    @Test
    @DisplayName("Add Vote - Voter Ineligible")
    void addVoteVoterIneligible() throws PollNotFoundException, PollingNotStartedException, PollingFinishedException,
        VoteAlreadyExistsException, UserCheckFailedException, VoterIneligibleException {
        Poll poll = pollBuilder.createPollEntity();
        poll.setEndDate(LocalDateTime.now().plusDays(1));
        VoteRequestDTO voteRequestDTO = voteBuilder.createVoteRequestDTO();

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(poll));
        Mockito.when(voteRepository.findByVoterIdAndPollId(Mockito.anyString(), Mockito.anyLong())).thenReturn(Optional.empty());
        Mockito.when(usersApiService.canUserVote(Mockito.anyString())).thenReturn(false);

        try {
            voteService.addVote(poll.getId(), voteRequestDTO);
        } catch (VoterIneligibleException ex) {
            Assertions.assertNotNull(ex);
            Assertions.assertEquals(ex.getMessage(), VoterIneligibleException.DEFAULT_MESSAGE);
        }
    }
    
}
