package com.avenuecode.leonardo.polling.service;

import com.avenuecode.leonardo.polling.builder.PollBuilder;
import com.avenuecode.leonardo.polling.builder.VoteBuilder;
import com.avenuecode.leonardo.polling.dto.PollDTO;
import com.avenuecode.leonardo.polling.dto.PollResultResponseDTO;
import com.avenuecode.leonardo.polling.entity.Poll;
import com.avenuecode.leonardo.polling.entity.Vote;
import com.avenuecode.leonardo.polling.exceptions.PollNotFoundException;
import com.avenuecode.leonardo.polling.exceptions.PollingAlreadyStartedException;
import com.avenuecode.leonardo.polling.exceptions.PollingFinishedException;
import com.avenuecode.leonardo.polling.repository.PollRepository;
import com.avenuecode.leonardo.polling.repository.VoteRepository;
import com.avenuecode.leonardo.polling.service.poll.PollService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@ActiveProfiles("test")
public class PollServiceTest {

    @MockBean
    private PollRepository pollRepository;

    @MockBean
    private VoteRepository voteRepository;

    @Autowired
    private PollService pollService;

    private VoteBuilder voteBuilder = new VoteBuilder();
    private PollBuilder pollBuilder = new PollBuilder();

    @Test
    @DisplayName("Search all polls - Success")
    void getAllPolls() {
        List<Poll> polls = pollBuilder.createListPollEntity();

        Mockito.when(pollRepository.findAll()).thenReturn(polls);

        List<PollDTO> result = pollService.getAllPolls();

        Assertions.assertNotNull(result);
        Assertions.assertNotEquals(result.size(), 0);

    }

    @Test
    @DisplayName("Search all polls by user - Success")
    void getAllPollsByUser() {
        List<Poll> polls = pollBuilder.createListPollEntity();

        Mockito.when(pollRepository.findAll()).thenReturn(polls);

        List<PollDTO> result = pollService.getAllPolls();

        Assertions.assertNotNull(result);
        Assertions.assertNotEquals(result.size(), 0);

    }

    @Test
    @DisplayName("Add poll - Success")
    void addPoll() {
        Poll poll = pollBuilder.createPollEntityAux();
        PollDTO newPoll = pollBuilder.createPollDTO();

        Mockito.when(pollRepository.save(Mockito.any(Poll.class))).thenReturn(poll);

        PollDTO result = pollService.addPoll(newPoll);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(result.getId(), poll.getId());
    }

    @Test
    @DisplayName("Update poll - Success")
    void updatePoll() throws PollNotFoundException {
        Poll poll = pollBuilder.createPollEntity();
        String originalName = poll.getName();
        PollDTO newPoll = pollBuilder.createPollDTO();
        newPoll.setName("Updated name");

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(poll));
        Mockito.when(pollRepository.save(Mockito.any(Poll.class))).thenReturn(poll);

        PollDTO result = pollService.updatePoll(newPoll.getId(), newPoll);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(result.getId(), newPoll.getId());
        Assertions.assertNotEquals(result.getName(), originalName);
    }

    @Test
    @DisplayName("Update poll - Not Found")
    void updatePollNotFound() {
        PollDTO poll = pollBuilder.createPollDTO();

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());

        try {
            pollService.updatePoll(poll.getId(), poll);
        } catch (PollNotFoundException ex) {
            Assertions.assertNotNull(ex);
            Assertions.assertEquals(ex.getMessage(), PollNotFoundException.DEFAULT_MESSAGE);
        }
    }

    @Test
    @DisplayName("Delete poll - Success")
    void deletePoll() throws PollNotFoundException {
        Poll poll = pollBuilder.createPollEntity();
        PollDTO pollDTO = pollBuilder.createPollDTO();

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(poll));
        Mockito.doNothing().when(pollRepository).delete(Mockito.any(Poll.class));

        pollService.deletePoll(pollDTO.getId());

        Mockito.verify(pollRepository, Mockito.times(1)).delete(Mockito.any(Poll.class));
    }

    @Test
    @DisplayName("Delete poll - Not Found")
    void deletePollNotFound() {
        PollDTO poll = pollBuilder.createPollDTO();

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());

        try {
            pollService.deletePoll(poll.getId());
        } catch (PollNotFoundException ex) {
            Assertions.assertNotNull(ex);
            Assertions.assertEquals(ex.getMessage(), PollNotFoundException.DEFAULT_MESSAGE);
        }
    }

    @Test
    @DisplayName("Start poll - Success")
    void startPoll() throws PollNotFoundException, PollingAlreadyStartedException, PollingFinishedException {
        Poll poll = pollBuilder.createPollEntityAux();

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(poll));
        Mockito.when(pollRepository.save(Mockito.any(Poll.class))).thenReturn(poll);

        PollDTO result = pollService.startPolling(poll.getId(), null);

        Assertions.assertNotNull(result);
        Assertions.assertNotNull(result.getEndDate());
    }

    @Test
    @DisplayName("Start poll with end date - Success")
    void startPollWithEndDate() throws PollNotFoundException, PollingAlreadyStartedException, PollingFinishedException {
        Poll poll = pollBuilder.createPollEntityAux();

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(poll));
        Mockito.when(pollRepository.save(Mockito.any(Poll.class))).thenReturn(poll);

        PollDTO result = pollService.startPolling(poll.getId(), LocalDateTime.now().plusMinutes(5));

        Assertions.assertNotNull(result);
        Assertions.assertNotNull(result.getEndDate());
    }

    @Test
    @DisplayName("Start poll - Already Started")
    void startPollAlreadyStarted() throws PollingFinishedException, PollNotFoundException {
        Poll poll = pollBuilder.createPollEntity();

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(poll));
        Mockito.when(pollRepository.save(Mockito.any(Poll.class))).thenReturn(poll);

        try {
            pollService.startPolling(poll.getId(), null);
        } catch (PollingAlreadyStartedException ex) {
            Assertions.assertNotNull(ex);
            Assertions.assertEquals(ex.getMessage(), PollingAlreadyStartedException.DEFAULT_MESSAGE);
        }
    }

    @Test
    @DisplayName("Start poll - Finished")
    void startPollFinished() throws PollingAlreadyStartedException, PollNotFoundException {
        Poll poll = pollBuilder.createPollEntity();
        poll.setEndDate(LocalDateTime.now().minusDays(1));

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(poll));
        Mockito.when(pollRepository.save(Mockito.any(Poll.class))).thenReturn(poll);

        try {
            pollService.startPolling(poll.getId(), null);
        } catch (PollingFinishedException ex) {
            Assertions.assertNotNull(ex);
            Assertions.assertEquals(ex.getMessage(), PollingFinishedException.DEFAULT_MESSAGE);
        }
    }

    @Test
    @DisplayName("Start poll - Not Found")
    void startPollNotFound() throws PollingFinishedException, PollingAlreadyStartedException {
        PollDTO poll = pollBuilder.createPollDTO();

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());

        try {
            pollService.startPolling(poll.getId(), null);
        } catch (PollNotFoundException ex) {
            Assertions.assertNotNull(ex);
            Assertions.assertEquals(ex.getMessage(), PollNotFoundException.DEFAULT_MESSAGE);
        }
    }

    @Test
    @DisplayName("Get poll results - Success")
    void getPollResults() throws PollNotFoundException {
        Poll poll = pollBuilder.createPollEntity();
        List<Vote> votes = voteBuilder.createListVoteEntity();

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(poll));
        Mockito.when(voteRepository.findAllByPollId(Mockito.anyLong())).thenReturn(votes);

        PollResultResponseDTO result = pollService.getPollResults(poll.getId());

        Assertions.assertNotNull(result);
        Assertions.assertEquals(result.getVotes().get("Yes"), 1);
    }

    @Test
    @DisplayName("Get poll results - Not Found")
    void getPollResultsNotFound() {
        Poll poll = pollBuilder.createPollEntity();

        Mockito.when(pollRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());

        try {
            pollService.getPollResults(poll.getId());
        } catch (PollNotFoundException ex) {
            Assertions.assertNotNull(ex);
            Assertions.assertEquals(ex.getMessage(), PollNotFoundException.DEFAULT_MESSAGE);
        }
    }
    
}
