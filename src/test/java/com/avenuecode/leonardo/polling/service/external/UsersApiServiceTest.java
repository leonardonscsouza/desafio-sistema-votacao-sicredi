package com.avenuecode.leonardo.polling.service.external;

import com.avenuecode.leonardo.polling.builder.VoteBuilder;
import com.avenuecode.leonardo.polling.dto.UsersApiResponseDTO;
import com.avenuecode.leonardo.polling.dto.VoteDTO;
import com.avenuecode.leonardo.polling.exceptions.external.UserCheckFailedException;
import com.avenuecode.leonardo.polling.service.external.usersapi.UsersApiServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;

@MockitoSettings(strictness = Strictness.LENIENT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
public class UsersApiServiceTest {

    @InjectMocks
    private UsersApiServiceImpl usersApiService;

    @Mock
    private RestTemplate restTemplate;

    private final VoteBuilder voteBuilder = new VoteBuilder();

    @Test
    @DisplayName("Search user rates - Success")
    void canUserVoteAble() throws UserCheckFailedException {
        VoteDTO voteDTO = voteBuilder.createVoteDTO();
        UsersApiResponseDTO usersApiResponseDTO = voteBuilder.createUsersApiResponseDTOAble();
        Mockito.when(restTemplate.exchange(Mockito.any(), Mockito.any(), Mockito.any(),
                        Mockito.any(ParameterizedTypeReference.class), Mockito.anyMap()))
                .thenReturn(new ResponseEntity<>(usersApiResponseDTO, HttpStatus.OK));

        boolean result = usersApiService.canUserVote(voteDTO.getVoterId());

        Assertions.assertEquals(result, true);
    }

    @Test
    @DisplayName("Search user rates - Success")
    void canUserVoteUnable() throws UserCheckFailedException {
        VoteDTO voteDTO = voteBuilder.createVoteDTO();
        UsersApiResponseDTO usersApiResponseDTO = voteBuilder.createUsersApiResponseDTOUnable();
        Mockito.when(restTemplate.exchange(Mockito.any(), Mockito.any(), Mockito.any(),
                        Mockito.any(ParameterizedTypeReference.class), Mockito.anyMap()))
                .thenReturn(new ResponseEntity<>(usersApiResponseDTO, HttpStatus.OK));

        boolean result = usersApiService.canUserVote(voteDTO.getVoterId());

        Assertions.assertEquals(result, false);
    }

    @Test
    @DisplayName("Search latest rates - Failed")
    void canUserVoteFailed() {
        VoteDTO voteDTO = voteBuilder.createVoteDTO();
        UsersApiResponseDTO usersApiResponseDTO = voteBuilder.createUsersApiResponseDTOAble();
        Mockito.when(restTemplate.exchange(Mockito.any(), Mockito.any(), Mockito.any(),
                        Mockito.any(ParameterizedTypeReference.class), Mockito.anyMap()))
                .thenThrow(NullPointerException.class);

        try {
            usersApiService.canUserVote(voteDTO.getVoterId());
        } catch (Exception ex) {
            Assertions.assertNotNull(ex);
        }
    }

    @Test
    @DisplayName("Search latest rates - Conversion Failed")
    void canUserVoteConversionFailed() {
        VoteDTO voteDTO = voteBuilder.createVoteDTO();
        UsersApiResponseDTO usersApiResponseDTO = voteBuilder.createUsersApiResponseDTOAble();
        Mockito.when(restTemplate.exchange(Mockito.any(), Mockito.any(), Mockito.any(),
                        Mockito.any(ParameterizedTypeReference.class), Mockito.anyMap()))
                .thenReturn(new ResponseEntity<>(null, HttpStatus.OK));

        try {
            usersApiService.canUserVote(voteDTO.getVoterId());
        } catch (UserCheckFailedException ex) {
            Assertions.assertNotNull(ex);
        }
    }

}
