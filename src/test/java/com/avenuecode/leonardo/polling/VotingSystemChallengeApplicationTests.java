package com.avenuecode.leonardo.polling;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class VotingSystemChallengeApplicationTests {

	@Test
	@DisplayName("Context load")
	void contextLoads() {
	}

}
