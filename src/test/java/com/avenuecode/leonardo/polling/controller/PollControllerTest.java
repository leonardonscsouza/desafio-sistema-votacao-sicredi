package com.avenuecode.leonardo.polling.controller;

import com.avenuecode.leonardo.polling.builder.PollBuilder;
import com.avenuecode.leonardo.polling.dto.PollDTO;
import com.avenuecode.leonardo.polling.dto.PollResultResponseDTO;
import com.avenuecode.leonardo.polling.dto.ResponseDTO;
import com.avenuecode.leonardo.polling.exceptions.PollNotFoundException;
import com.avenuecode.leonardo.polling.service.poll.PollService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class PollControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PollService pollService;

    private static final String POLLS_URL = "/api/v1.0/polls";

    private PollBuilder pollBuilder = new PollBuilder();

    private ObjectMapper om;

    @BeforeAll
    void prepareTests() {
        om = new ObjectMapper();
        om.findAndRegisterModules();
    }

    @Test
    @DisplayName("GET - Search all polls - Success")
    void getAllPolls() throws Exception {
        List<PollDTO> polls = pollBuilder.createListPollDTO();

        Mockito.when(pollService.getAllPolls()).thenReturn(polls);

        om.readValue(mockMvc.perform(get(POLLS_URL)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.*", hasSize((2))))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString(), ResponseDTO.class);
    }

    @Test
    @DisplayName("POST - Include poll - Success")
    void includePoll() throws Exception {
        PollDTO pollDTO = pollBuilder.createPollDTO();

        Mockito.when(pollService.addPoll(Mockito.any(PollDTO.class))).thenReturn(pollDTO);

        om.readValue(mockMvc.perform(post(POLLS_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(pollDTO)))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.id", is((int) pollDTO.getId())))
                .andExpect(status().isCreated()).andReturn().getResponse().getContentAsString(), ResponseDTO.class);
    }

    @Test
    @DisplayName("PUT - Update poll - Success")
    void updatePoll() throws Exception {
        PollDTO pollOriginalDTO = pollBuilder.createPollDTO();
        PollDTO pollDTO = pollBuilder.createPollDTO();
        pollDTO.setName("Updated name");

        Mockito.when(pollService.updatePoll(Mockito.any(Long.class), Mockito.any(PollDTO.class)))
                .thenReturn(pollDTO);

        om.readValue(mockMvc.perform(put(POLLS_URL + "/" + pollDTO.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(pollDTO)))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.name", not(pollOriginalDTO.getName())))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString(), ResponseDTO.class);
    }

    @Test
    @DisplayName("PUT - Update poll - Not Found")
    void updatePollNotFound() throws Exception {
        PollDTO pollDTO = pollBuilder.createPollDTO();
        pollDTO.setName("Updated name");

        Mockito.doThrow(new PollNotFoundException())
                .when(pollService).updatePoll(Mockito.any(Long.class), Mockito.any(PollDTO.class));

        om.readValue(mockMvc.perform(put(POLLS_URL + "/" + pollDTO.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(pollDTO)))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorMessage", not(nullValue())))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString(), ResponseDTO.class);
    }

    @Test
    @DisplayName("DELETE - Delete poll - Success")
    void deletePoll() throws Exception {
        PollDTO pollDTO = pollBuilder.createPollDTOAux();

        Mockito.doNothing().when(pollService).deletePoll(Mockito.any(Long.class));

        om.readValue(mockMvc.perform(delete(POLLS_URL + "/" + pollDTO.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(pollDTO)))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data", not(nullValue())))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString(), ResponseDTO.class);
    }

    @Test
    @DisplayName("DELETE - Delete poll - Not Found")
    void deletePollNotFound() throws Exception {
        PollDTO pollDTO = pollBuilder.createPollDTO();

        Mockito.doThrow(new PollNotFoundException())
                .when(pollService).deletePoll(Mockito.any(Long.class));

        om.readValue(mockMvc.perform(delete(POLLS_URL + "/" + pollDTO.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(pollDTO)))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorMessage", not(nullValue())))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString(), ResponseDTO.class);
    }

    @Test
    @DisplayName("POST - Start polling - Success")
    void startPolling() throws Exception {
        PollDTO pollDTO = pollBuilder.createPollDTO();

        Mockito.when(pollService.startPolling(Mockito.any(Long.class), Mockito.any()))
                .thenReturn(pollDTO);

        om.readValue(mockMvc.perform(put(POLLS_URL + "/" + pollDTO.getId() + "/start")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(pollDTO)))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.name", is(pollDTO.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.startingDate", not(nullValue())))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString(), ResponseDTO.class);
    }

    @Test
    @DisplayName("POST - Start polling - Not Found")
    void startPollingNotFound() throws Exception {
        PollDTO pollDTO = pollBuilder.createPollDTO();

        Mockito.doThrow(new PollNotFoundException())
                .when(pollService).startPolling(Mockito.any(Long.class), Mockito.any());

        om.readValue(mockMvc.perform(put(POLLS_URL + "/" + pollDTO.getId() + "/start")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(pollDTO)))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorMessage", not(nullValue())))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString(), ResponseDTO.class);
    }

    @Test
    @DisplayName("GET - Get poll results - Success")
    void getPollResults() throws Exception {
        PollDTO pollDTO = pollBuilder.createPollDTO();
        PollResultResponseDTO pollResultResponseDTO = pollBuilder.createPollResultResponseDTO();

        Mockito.when(pollService.getPollResults(Mockito.anyLong())).thenReturn(pollResultResponseDTO);

        om.readValue(mockMvc.perform(get(POLLS_URL + "/" + pollDTO.getId() + "/result")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.*", hasSize((1))))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString(), ResponseDTO.class);
    }

}
