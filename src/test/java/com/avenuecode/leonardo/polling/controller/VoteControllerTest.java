package com.avenuecode.leonardo.polling.controller;

import com.avenuecode.leonardo.polling.builder.PollBuilder;
import com.avenuecode.leonardo.polling.builder.VoteBuilder;
import com.avenuecode.leonardo.polling.dto.PollDTO;
import com.avenuecode.leonardo.polling.dto.ResponseDTO;
import com.avenuecode.leonardo.polling.dto.VoteDTO;
import com.avenuecode.leonardo.polling.dto.VoteRequestDTO;
import com.avenuecode.leonardo.polling.exceptions.PollNotFoundException;
import com.avenuecode.leonardo.polling.service.vote.VoteService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class VoteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VoteService voteService;

    private static final String POLLS_URL = "/api/v1.0/votes";

    private VoteBuilder voteBuilder = new VoteBuilder();

    private PollBuilder pollBuilder = new PollBuilder();

    private ObjectMapper om;

    @BeforeAll
    void prepareTests() {
        om = new ObjectMapper();
        om.findAndRegisterModules();
    }

    @Test
    @DisplayName("GET - Search all votes for poll - Success")
    void getAllVotes() throws Exception {
        PollDTO pollDTO = pollBuilder.createPollDTO();
        List<VoteDTO> votes = voteBuilder.createListVoteDTO();

        Mockito.when(voteService.getAllVotesPerPoll(Mockito.anyLong())).thenReturn(votes);

        om.readValue(mockMvc.perform(get(POLLS_URL + "/poll/" + pollDTO.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.*", hasSize((2))))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString(), ResponseDTO.class);
    }

    @Test
    @DisplayName("POST - Include vote - Success")
    void includeVote() throws Exception {
        VoteDTO voteDTO = voteBuilder.createVoteDTO();

        Mockito.when(voteService.addVote(Mockito.anyLong(), Mockito.any(VoteRequestDTO.class))).thenReturn(voteDTO);

        om.readValue(mockMvc.perform(post(POLLS_URL + "/poll/" + voteDTO.getPoll().getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(voteDTO)))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.id", is((int) voteDTO.getId())))
                .andExpect(status().isCreated()).andReturn().getResponse().getContentAsString(), ResponseDTO.class);
    }

    @Test
    @DisplayName("POST - Include vote - Poll Not Found")
    void updateVoteNotFound() throws Exception {
        VoteDTO voteDTO = voteBuilder.createVoteDTO();

        Mockito.doThrow(new PollNotFoundException())
                .when(voteService).addVote(Mockito.anyLong(), Mockito.any(VoteRequestDTO.class));

        om.readValue(mockMvc.perform(post(POLLS_URL + "/poll/" + voteDTO.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(voteDTO)))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorMessage", not(nullValue())))
                .andExpect(status().isNotFound()).andReturn().getResponse().getContentAsString(), ResponseDTO.class);
    }

}
