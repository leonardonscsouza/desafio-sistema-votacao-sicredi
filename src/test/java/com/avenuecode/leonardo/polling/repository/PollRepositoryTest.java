package com.avenuecode.leonardo.polling.repository;

import com.avenuecode.leonardo.polling.builder.PollBuilder;
import com.avenuecode.leonardo.polling.entity.Poll;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@ActiveProfiles("test")
public class PollRepositoryTest {

    @Autowired
    private PollRepository pollRepository;

    private final PollBuilder pollBuilder = new PollBuilder();

    @BeforeAll
    public void init() {
        pollRepository.save(pollBuilder.createPollEntity());
        pollRepository.save(pollBuilder.createPollEntityAux());
    }

    @Test
    @DisplayName("Find all polls")
    public void testFindAll() {
        List<Poll> pollsFound = pollRepository.findAll();
        Assertions.assertNotNull(pollsFound);
        Assertions.assertNotEquals(pollsFound.size(), 0);
    }

    @Test
    @DisplayName("Find poll by id")
    public void testFindById() {
        Poll poll = pollBuilder.createPollEntity();
        Optional<Poll> pollFound = pollRepository.findById(poll.getId());
        Assertions.assertTrue(pollFound.isPresent());
    }

    @Test
    @DisplayName("Add poll")
    public void testAddPoll() {
        Poll newPoll = new Poll();
        newPoll.setId(3L);
        newPoll.setName("Add new Poll");
        newPoll = pollRepository.save(newPoll);
        Assertions.assertNotNull(newPoll);
    }

    @Test
    @DisplayName("Update poll")
    public void testUpdatePoll() {
        Poll poll = pollBuilder.createPollEntity();
        Optional<Poll> pollFound = pollRepository.findById(poll.getId());
        Assertions.assertTrue(pollFound.isPresent());
        Poll updatedPoll = pollFound.get();
        updatedPoll.setName("Updated name");
        updatedPoll = pollRepository.save(updatedPoll);
        Assertions.assertNotEquals(poll.getName(), updatedPoll.getName());
    }

    @Test
    @DisplayName("Delete poll")
    public void testDeletePoll() {
        Poll poll = pollBuilder.createPollEntityAux();
        Optional<Poll> pollFound = pollRepository.findById(poll.getId());
        pollRepository.delete(pollFound.get());
        pollFound = pollRepository.findById(poll.getId());
        Assertions.assertFalse(pollFound.isPresent());
    }

}
