package com.avenuecode.leonardo.polling.repository;

import com.avenuecode.leonardo.polling.builder.PollBuilder;
import com.avenuecode.leonardo.polling.builder.VoteBuilder;
import com.avenuecode.leonardo.polling.entity.Vote;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
@ActiveProfiles("test")
public class VoteRepositoryTest {

    @Autowired
    private VoteRepository voteRepository;

    @Autowired
    private PollRepository pollRepository;

    private final PollBuilder pollBuilder = new PollBuilder();

    private final VoteBuilder voteBuilder = new VoteBuilder();

    @BeforeAll
    public void init() {
        pollRepository.save(pollBuilder.createPollEntity());
        Vote firstVote = voteBuilder.createVoteEntity();
        Vote secondVote = voteBuilder.createVoteEntityAux();
        voteRepository.save(firstVote);
        voteRepository.save(secondVote);
    }

    @Test
    @DisplayName("Search votes by poll - Success")
    public void testFindVotesByPoll() {
        Vote vote = voteBuilder.createVoteEntity();
        List<Vote> listVotesByPoll = voteRepository.findAllByPollId(vote.getPoll().getId());
        Assertions.assertEquals(2, listVotesByPoll.size());
    }

    @Test
    @DisplayName("Search vote by voter and poll - Success")
    public void testFindVotesByVoterAndPoll() {
        Vote vote = voteBuilder.createVoteEntity();
        Optional<Vote> listVotesByPoll = voteRepository.findByVoterIdAndPollId(vote.getVoterId(), vote.getPoll().getId());
        Assertions.assertTrue(listVotesByPoll.isPresent());
        Assertions.assertEquals(vote.getId(), listVotesByPoll.get().getId());
    }

}
